#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <ctype.h>
#include <string.h>

#define MAX_TREE_HT 50

// A Huffman tree node
struct MinHeapNode {
 
    // One of the input characters
    char data;
 
    // Frequency of the character
    unsigned freq;
 
    // Left and right child of this node
    struct MinHeapNode *left, *right;
};
 
// A Min Heap:  Collection of
// min-heap (or Huffman tree) nodes
struct MinHeap {
 
    // Current size of min heap
    unsigned size;
 
    // capacity of min heap
    unsigned capacity;
 
    // Array of minheap node pointers
    struct MinHeapNode** array;
};
 
// A utility function allocate a new
// min heap node with given character
// and frequency of the character
struct MinHeapNode* newNode(char data, unsigned freq)
{
    struct MinHeapNode* temp = (struct MinHeapNode*)malloc(
        sizeof(struct MinHeapNode));
 
    temp->left = temp->right = NULL;
    temp->data = data;
    temp->freq = freq;
 
    return temp;
}
 
// A utility function to create
// a min heap of given capacity
struct MinHeap* createMinHeap(unsigned capacity)
 
{
 
    struct MinHeap* minHeap
        = (struct MinHeap*)malloc(sizeof(struct MinHeap));
 
    // current size is 0
    minHeap->size = 0;
 
    minHeap->capacity = capacity;
 
    minHeap->array = (struct MinHeapNode**)malloc(
        minHeap->capacity * sizeof(struct MinHeapNode*));
    return minHeap;
}
 
// A utility function to
// swap two min heap nodes
void swapMinHeapNode(struct MinHeapNode** a,
                     struct MinHeapNode** b)
 
{
 
    struct MinHeapNode* t = *a;
    *a = *b;
    *b = t;
}
 
// The standard minHeapify function.
void minHeapify(struct MinHeap* minHeap, int idx)
 
{
 
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;
 
    if (left < minHeap->size
        && minHeap->array[left]->freq
               < minHeap->array[smallest]->freq)
        smallest = left;
 
    if (right < minHeap->size
        && minHeap->array[right]->freq
               < minHeap->array[smallest]->freq)
        smallest = right;
 
    if (smallest != idx) {
        swapMinHeapNode(&minHeap->array[smallest],
                        &minHeap->array[idx]);
        minHeapify(minHeap, smallest);
    }
}
 
// A utility function to check
// if size of heap is 1 or not
int isSizeOne(struct MinHeap* minHeap)
{
 
    return (minHeap->size == 1);
}
 
// A standard function to extract
// minimum value node from heap
struct MinHeapNode* extractMin(struct MinHeap* minHeap)
 
{
 
    struct MinHeapNode* temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];
 
    --minHeap->size;
    minHeapify(minHeap, 0);
 
    return temp;
}
 
// A utility function to insert
// a new node to Min Heap
void insertMinHeap(struct MinHeap* minHeap,
                   struct MinHeapNode* minHeapNode)
 
{
 
    ++minHeap->size;
    int i = minHeap->size - 1;
 
    while (i
           && minHeapNode->freq
                  < minHeap->array[(i - 1) / 2]->freq) {
 
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
 
    minHeap->array[i] = minHeapNode;
}
 
// A standard function to build min heap
void buildMinHeap(struct MinHeap* minHeap)
 
{
 
    int n = minHeap->size - 1;
    int i;
 
    for (i = (n - 1) / 2; i >= 0; --i)
        minHeapify(minHeap, i);
}

 
// Utility function to check if this node is leaf
int isLeaf(struct MinHeapNode* root)
 
{
 
    return !(root->left) && !(root->right);
}
 
// Creates a min heap of capacity
// equal to size and inserts all character of
// data[] in min heap. Initially size of
// min heap is equal to capacity
struct MinHeap* createAndBuildMinHeap(char data[],
                                      int freq[], int size)
 
{
 
    struct MinHeap* minHeap = createMinHeap(size);
 
    for (int i = 0; i < size; ++i)
        minHeap->array[i] = newNode(data[i], freq[i]);
 
    minHeap->size = size;
    buildMinHeap(minHeap);
 
    return minHeap;
}
 
// The main function that builds Huffman tree
struct MinHeapNode* buildHuffmanTree(char data[],
                                     int freq[], int size)
 
{
    struct MinHeapNode *left, *right, *top;
 
    // Step 1: Create a min heap of capacity
    // equal to size.  Initially, there are
    // modes equal to size.
    struct MinHeap* minHeap
        = createAndBuildMinHeap(data, freq, size);
 
    // Iterate while size of heap doesn't become 1
    while (!isSizeOne(minHeap)) {
 
        // Step 2: Extract the two minimum
        // freq items from min heap
        left = extractMin(minHeap);
        right = extractMin(minHeap);
 
        // Step 3:  Create a new internal
        // node with frequency equal to the
        // sum of the two nodes frequencies.
        // Make the two extracted node as
        // left and right children of this new node.
        // Add this node to the min heap
        // '$' is a special value for internal nodes, not
        // used
        top = newNode('$', left->freq + right->freq);
 
        top->left = left;
        top->right = right;
 
        insertMinHeap(minHeap, top);
    }
 
    // Step 4: The remaining node is the
    // root node and the tree is complete.
    return extractMin(minHeap);
}

char kal[30][15];
int count = -1;
// A utility function to print an array of size n
void printArr(int arr[], int n, int count)
{
    int i;
    for (i = 0; i < n; i++){
        sprintf(&kal[count][i + 3], "%d", arr[i]);
    }
}
 
// Prints huffman codes from the root of Huffman Tree.
// It uses arr[] to store codes
void printCodes(struct MinHeapNode* root, int arr[],
                int top)
 
{   
 
    // Assign 0 to left edge and recur
    if (root->left) {
 
        arr[top] = 0;
        printCodes(root->left, arr, top + 1);
    }
 
    // Assign 1 to right edge and recur
    if (root->right) {
 
        arr[top] = 1;
        printCodes(root->right, arr, top + 1);
    }

    char str[5];
 
    // If this is a leaf node, then
    // it contains one of the input
    // characters, print the character
    // and its code from arr[]
    if (isLeaf(root)) {
        
        count++;

        sprintf(str, "%c: ", root->data);
        sprintf(kal[count], "%s", str);

        printArr(arr, top, count);

    }
}
 
// The main function that builds a
// Huffman Tree and print codes by traversing
// the built Huffman Tree
void HuffmanCodes(char data[], int freq[], int size)
 
{
    // Construct Huffman Tree
    struct MinHeapNode* root = buildHuffmanTree(data, freq, size);
 
    // Print Huffman codes using
    // the Huffman tree built above
    int arr[MAX_TREE_HT], top = 0;
 
    printCodes(root, arr, top);
}


typedef struct {
    char x;
    int jumlah;
} Huruf;

void hitung_huruf(int fd){

    
    Huruf h[30];
    size_t struct_size = sizeof(h);

    char ch;

    FILE *file;
    file = fopen("file.txt", "r");

    if(file == NULL) puts("File tidak ditemukan atau tidak dapat dibuka");

    char alfabet[] = {
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 
        'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 
        'U', 'V', 'W', 'X', 'Y', 'Z'
    };


    for(int i = 0; i < 26; i++){

        h[i].x = alfabet[i];
        h[i].jumlah = 0;
    }


    while((ch = fgetc(file)) != EOF){

        if((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122)){
            
            ch = toupper(ch);
            for(int i = 0;i < 26; i++){

                if(ch == h[i].x){
                    h[i].jumlah++;
                    break;
                }
            }

        }
    }

    write(fd, h, struct_size); //write pipe

    // for(int i = 0; i < 26; i++){
    //     printf("%c : %d\n", h[i].x, h[i].jumlah);
    // }
}

typedef struct{
    char s;
    char huffman[20];
    int length;
} k;

int main(int argc, char* argv[]){

    int fd[2];
    int fd2[2];
    pid_t pid;

    k struct_data[30];

    if(pipe(fd) == -1){
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    if(pipe(fd2) == -1){
        perror("pipe2");
        exit(EXIT_FAILURE);
    }

    pid = fork();
    if(pid == -1){
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if(pid == 0){ //child process

        Huruf h2[26];
        close(fd[1]); //menutup write end of pipe
        read(fd[0], h2, sizeof(h2));
        close(fd[0]);

        char items[26];
        int freqs[26];
        int j = 0;
        for (int i = 0; i < 26; i++) {
            if (h2[i].jumlah > 0) {
                items[j] = h2[i].x;
                freqs[j] = h2[i].jumlah;
                j++;
            }
        }
        HuffmanCodes(items, freqs, j);
        
        for (int i = 0; i < 26; i++) {

            if (write(fd2[1], kal[i], strlen(kal[i])) < 0) {
                perror("Error writing to pipe");
                exit(EXIT_FAILURE);
            }

            if (write(fd2[1], "\n", 1) < 0) {
                perror("Error writing to pipe");
                exit(EXIT_FAILURE);
            }

            usleep(1000);
        }

        close(fd2[1]);
    }

    else{

        close(fd[0]); //menutup read end of pipe
        hitung_huruf(fd[1]);

        close(fd[1]);

        char buffer[1024];
        int bytes_read;
        char data[50][50];
        char newdata[50][50];

        int row = 0;
        int x = 23;
        while (x--) {
            bytes_read = read(fd2[0], buffer, sizeof(buffer));
            // printf("%.*s", bytes_read, buffer);

            //menyimpan nilai buffer ke array 2 dimensi
            memcpy(data[row], buffer, bytes_read);
            data[row][bytes_read] = '\0'; // Menambahkan nul-terminator

            // Mencari substring pada setiap baris dan menyalinnya ke dalam array `newdata`
            char* substr = strstr(data[row], ": ");

            if(substr != NULL) {
                substr += 2; // Melompati ": "
                strncpy(newdata[row], substr, 50 - 1);
                newdata[row][50 - 1] = '\0'; // Menambahkan nul-terminator

                // Menghilangkan newline pada newdata[row]
                char* newline_pos = strchr(newdata[row], '\n');
                if (newline_pos != NULL) {
                    *newline_pos = '\0';
                }

                // Menyimpan nilai ke dalam objek struct
                struct_data[row].s = data[row][0];
                strncpy(struct_data[row].huffman, newdata[row], sizeof(struct_data[row].huffman) - 1);
                struct_data[row].huffman[sizeof(struct_data[row].huffman) - 1] = '\0';
            }

            struct_data[row].length = strlen(struct_data[row].huffman);

            row++;
        }

        if (bytes_read == -1) {
            perror("Error reading from pipe");
            exit(EXIT_FAILURE);
        }

        close(fd2[0]);

        // puts("masuk sini");
        for(int i = 0; i < 23; i++){
            printf("%s", data[i]);
        }

        // puts("kesini");

        FILE *file;
        file = fopen("file.txt", "r");

        FILE *newfile = fopen("newfile.txt", "w");
        if (newfile == NULL) {
            printf("Error opening newfile.txt\n");
            exit(1);
        }

        if(file == NULL) puts("File tidak ditemukan atau tidak dapat dibuka");

        char ch;
        while((ch = fgetc(file)) != EOF){

            if((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122)){

                ch = toupper(ch);
                for(int i = 0; i < 26; i++){
                    if(ch == struct_data[i].s){
                            
                        fprintf(newfile, "%s ", struct_data[i].huffman);
                    }
                }
            }
        }

        // for(int i = 0; i < 23; i++){
        //     printf("Huruf: %c\n", struct_data[i].s);
        //     printf("Huffman: %s\n", struct_data[i].huffman);
        //     printf("Panjang: %d\n", struct_data[i].length);
        // }

        file = fopen("file.txt", "r");

        int bit_before = 0, bit_after = 0;
        while((ch = fgetc(file)) != EOF){

            if((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122)){
                bit_before += 8;

                ch = toupper(ch);

                for(int i = 0; i < 26; i++){

                    if(ch == struct_data[i].s){
                        bit_after += struct_data[i].length;

                        break;
                    }
                }
            }
            else continue;
        }

        printf("Jumlah bit sebelumnya: %d\n", bit_before);
        printf("Jumlah bit setelahnya: %d\n", bit_after);

    }


    return 0;
}

/**
 * Referensi:
 * https://www.geeksforgeeks.org/huffman-coding-greedy-algo-3/
*/