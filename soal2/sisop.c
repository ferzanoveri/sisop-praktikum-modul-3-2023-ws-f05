#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define ROW_1 4
#define COL_2 5

unsigned long long faktorial(int a){
    unsigned long long sum = 1;
    for (int i = 1; i <= a; i++)
    {
        sum = sum * i;
    }
    return sum;
}

int main() {

    clock_t start, end;
    double cpu_time_used;

    start = clock();

    int i, j;
    int shmid;
    key_t key = 1234;
    int (*result)[COL_2];
    
    // get shared memory ID
    if ((shmid = shmget(key, sizeof(int[ROW_1][COL_2]), 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    
    // attach shared memory
    if ((result = shmat(shmid, NULL, 0)) == (int (*)[COL_2]) -1) {
        perror("shmat");
        exit(1);
    }
    
    // print the result matrix
    printf("Result:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    printf("Faktorial:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            printf("%llu ", faktorial(result[i][j]));
        }
        printf("\n");
    }

    // detach shared memory
    shmdt(result);

    end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("\nWaktu eksekusi: %f detik\n", cpu_time_used);
    
    return 0;
}
