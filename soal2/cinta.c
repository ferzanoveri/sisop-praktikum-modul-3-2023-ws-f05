#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <time.h>

#define ROW_1 4
#define COL_2 5
#define NUM_THREADS ROW_1 * COL_2

unsigned long long faktorial(int a){
    unsigned long long sum = 1;
    for (int i = 1; i <= a; i++)
    {
        sum = sum * i;
    }
    return sum;
}

void *hitung_faktorial(void *arg) {
    int *data = (int *) arg;
    unsigned long long *hasil = malloc(sizeof(unsigned long long));
    *hasil = faktorial(*data);
    pthread_exit(hasil);
}

int main() {

    clock_t start, end;
    double cpu_time_used;

    start = clock();

    int i, j, rc;
    int shmid;
    key_t key = 1234;
    int (*result)[COL_2];
    pthread_t threads[NUM_THREADS];
    void *status;
    int thread_count = 0;
    
    // get shared memory ID
    if ((shmid = shmget(key, sizeof(int[ROW_1][COL_2]), 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    
    // attach shared memory
    if ((result = shmat(shmid, NULL, 0)) == (int (*)[COL_2]) -1) {
        perror("shmat");
        exit(1);
    }
    
    // print the result matrix
    printf("Result:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    // hitung faktorial menggunakan thread
    printf("Faktorial:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            int *data = malloc(sizeof(int));
            *data = result[i][j];
            rc = pthread_create(&threads[thread_count], NULL, hitung_faktorial, (void *) data);
            if (rc) {
                printf("Error: return code from pthread_create() is %d\n", rc);
                exit(1);
            }
            thread_count++;
        }
    }
    // join semua thread dan print hasil faktorial
    for (i = 0; i < NUM_THREADS; i++) {
        rc = pthread_join(threads[i], &status);
        if (rc) {
            printf("Error: return code from pthread_join() is %d\n", rc);
            exit(1);
        }
        printf("%llu ", *(unsigned long long *) status);
        free(status);
        if ((i + 1) % COL_2 == 0) {
            printf("\n");
        }
    }

    // detach shared memory
    shmdt(result);
    
    end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("\nWaktu eksekusi: %f detik\n", cpu_time_used);

    return 0;
}
