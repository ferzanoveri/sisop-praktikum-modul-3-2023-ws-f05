#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW_1 4
#define COL_1 2
#define ROW_2 2
#define COL_2 5

int main() {
    int i, j, k;
    int mat1[ROW_1][COL_1], mat2[ROW_2][COL_2], result[ROW_1][COL_2];

    // generate random value untuk matriks pertama
    srand(time(NULL));
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_1; j++) {
            mat1[i][j] = rand() % 5 + 1;  // random number between 1-5
        }
    }

    // generate random value untuk matriks kedua
    for (i = 0; i < ROW_2; i++) {
        for (j = 0; j < COL_2; j++) {
            mat2[i][j] = rand() % 4 + 1;  // random number between 1-4
        }
    }

    // melakukan perkalian matriks
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            result[i][j] = 0;
            for (k = 0; k < ROW_2; k++) {
                result[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }

    // membuat shared memory
    key_t key = 1234;
    int shmid = shmget(key, sizeof(result), 0666|IPC_CREAT);
    int *shmaddr = (int*) shmat(shmid, (void*)0, 0);

    // mengcopy hasil perkalian matriks ke shared memory
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            *(shmaddr + i * COL_2 + j) = result[i][j];
        }
    }

    // print the result matrix
    printf("Result:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // detach shared memory
    shmdt(shmaddr);

    return 0;
}
