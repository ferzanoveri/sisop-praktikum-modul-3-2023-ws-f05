# sisop-praktikum-modul-3-2023-WS-F05

| NAMA  | NRP |
| ------------- | ------------- |
| Azhar Abiyu Rasendriya Harun  | 5025211177  |
| Beauty Valen Fajri  | 5025211227 |
| Ferza Noveri  | 5025211097 |

## NOMOR 1
#### Deskripsi problem
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! (Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).

#### Point A
Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.

#### Code
```shell
void hitung_huruf(int fd){

    
    Huruf h[30];
    size_t struct_size = sizeof(h);

    char ch;

    FILE *file;
    file = fopen("file.txt", "r");

    if(file == NULL) puts("File tidak ditemukan atau tidak dapat dibuka");

    char alfabet[] = {
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 
        'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 
        'U', 'V', 'W', 'X', 'Y', 'Z'
    };


    for(int i = 0; i < 26; i++){

        h[i].x = alfabet[i];
        h[i].jumlah = 0;
    }


    while((ch = fgetc(file)) != EOF){

        if((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122)){
            
            ch = toupper(ch);
            for(int i = 0;i < 26; i++){

                if(ch == h[i].x){
                    h[i].jumlah++;
                    break;
                }
            }

        }
    }

    write(fd, h, struct_size); //write pipe

    // for(int i = 0; i < 26; i++){
    //     printf("%c : %d\n", h[i].x, h[i].jumlah);
    // }
}
```
#### Penjelasan
Menggunakan algoritma huffman untuk proses kompresi lossless

#### Point B
Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.

#### Code
```shell
void HuffmanCodes(char data[], int freq[], int size)
 
{
    // Construct Huffman Tree
    struct MinHeapNode* root = buildHuffmanTree(data, freq, size);
 
    // Print Huffman codes using
    // the Huffman tree built above
    int arr[MAX_TREE_HT], top = 0;
 
    printCodes(root, arr, top);
}
```
#### Penjelasan
Menggunakan pipes dan fork di shild proses


#### Point C
Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.

#### Code
```shell
 else{

        close(fd[0]); //menutup read end of pipe
        hitung_huruf(fd[1]);

        close(fd[1]);

        char buffer[1024];
        int bytes_read;
        char data[50][50];
        char newdata[50][50];

        int row = 0;
        int x = 23;
        while (x--) {
            bytes_read = read(fd2[0], buffer, sizeof(buffer));
            // printf("%.*s", bytes_read, buffer);

            //menyimpan nilai buffer ke array 2 dimensi
            memcpy(data[row], buffer, bytes_read);
            data[row][bytes_read] = '\0'; // Menambahkan nul-terminator

            // Mencari substring pada setiap baris dan menyalinnya ke dalam array `newdata`
            char* substr = strstr(data[row], ": ");

            if(substr != NULL) {
                substr += 2; // Melompati ": "
                strncpy(newdata[row], substr, 50 - 1);
                newdata[row][50 - 1] = '\0'; // Menambahkan nul-terminator

                // Menghilangkan newline pada newdata[row]
                char* newline_pos = strchr(newdata[row], '\n');
                if (newline_pos != NULL) {
                    *newline_pos = '\0';
                }

                // Menyimpan nilai ke dalam objek struct
                struct_data[row].s = data[row][0];
                strncpy(struct_data[row].huffman, newdata[row], sizeof(struct_data[row].huffman) - 1);
                struct_data[row].huffman[sizeof(struct_data[row].huffman) - 1] = '\0';
            }

            struct_data[row].length = strlen(struct_data[row].huffman);

            row++;
        }

        if (bytes_read == -1) {
            perror("Error reading from pipe");
            exit(EXIT_FAILURE);
        }

        close(fd2[0]);
```
#### Penjelasan
Pada parent process, kita gunakan untuk baca file yang akan dikompresi lalu kita hitung frekuensi kemunculan huruf pada file tersebut. Lalu, kirim hasil perhitungan frekuensi tiap huruf ke child process

#### Point D
Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 

#### Code
```shell
if(pid == 0){ //child process

        Huruf h2[26];
        close(fd[1]); //menutup write end of pipe
        read(fd[0], h2, sizeof(h2));
        close(fd[0]);

        char items[26];
        int freqs[26];
        int j = 0;
        for (int i = 0; i < 26; i++) {
            if (h2[i].jumlah > 0) {
                items[j] = h2[i].x;
                freqs[j] = h2[i].jumlah;
                j++;
            }
        }
        HuffmanCodes(items, freqs, j);
        
        for (int i = 0; i < 26; i++) {

            if (write(fd2[1], kal[i], strlen(kal[i])) < 0) {
                perror("Error writing to pipe");
                exit(EXIT_FAILURE);
            }

            if (write(fd2[1], "\n", 1) < 0) {
                perror("Error writing to pipe");
                exit(EXIT_FAILURE);
            }

            usleep(1000);
        }

        close(fd2[1]);
    }
```
#### Penjelasan
Pada child process, kita simpan huffman tree. Untuk tiap huruf pada file, kita ubah karakternya menjadi kode huffman dan kita kirim ke pipe lalu kita kirim ke parent process.

#### Point E
Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

#### Code
```shell
        file = fopen("file.txt", "r");

        int bit_before = 0, bit_after = 0;
        while((ch = fgetc(file)) != EOF){

            if((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122)){
                bit_before += 8;

                ch = toupper(ch);

                for(int i = 0; i < 26; i++){

                    if(ch == struct_data[i].s){
                        bit_after += struct_data[i].length;

                        break;
                    }
                }
            }
            else continue;
        }

        printf("Jumlah bit sebelumnya: %d\n", bit_before);
        printf("Jumlah bit setelahnya: %d\n", bit_after);

    }


    return 0;
}
```
#### Penjelasan
Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algo huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algo huffman

## NOMOR 2
#### Point A
#### Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks dengan ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5.

```shell
define ROW_1 4
define COL_1 2
define ROW_2 2
define COL_2 5
```
Kode tersebut adalah bagian dari program dalam bahasa pemrograman C yang melakukan perkalian matriks menggunakan shared memory. Bagian kode tersebut digunakan untuk menyertakan beberapa header file yang dibutuhkan dalam program, yaitu:

- Baris pertama mendefinisikan konstanta ROW_1 sebagai nilai 4, konstanta ini kemungkinan menunjukkan jumlah baris pada matriks pertama dalam operasi perkalian matriks.
- Baris kedua mendefinisikan konstanta COL_1 sebagai nilai 2, konstanta ini kemungkinan menunjukkan jumlah kolom pada matriks pertama dan jumlah baris pada matriks kedua dalam operasi perkalian matriks.
- Baris ketiga mendefinisikan konstanta ROW_2 sebagai nilai 2, konstanta ini kemungkinan menunjukkan jumlah baris pada matriks kedua dalam operasi perkalian matriks.
- Baris keempat mendefinisikan konstanta COL_2 sebagai nilai 5, konstanta ini kemungkinan menunjukkan jumlah kolom pada matriks kedua dalam operasi perkalian matriks.

```shell
int main() {
    int i, j, k;
    int mat1[ROW_1][COL_1], mat2[ROW_2][COL_2], result[ROW_1][COL_2];
```
Bagian kode tersebut adalah deklarasi variabel-variabel yang akan digunakan dalam program. 

Untuk `int i, j, k;`, `variabel i, j, dan k` bertipe data `int` akan digunakan sebagai variabel loop untuk melakukan operasi pada matriks. 
Untuk `int mat1[ROW_1][COL_1]`, `mat2[ROW_2][COL_2]`, `result[ROW_1][COL_2];` deklarasi tiga matriks, yaitu `mat1` dengan ukuran `ROW_1 x COL_1`, `mat2` dengan ukuran `ROW_2 x COL_2`, dan `result` dengan ukuran `ROW_1 x COL_2`. Variabel-variabel tersebut akan digunakan untuk menyimpan nilai matriks yang akan digunakan dalam operasi perkalian matriks pada program.

```shell
    // generate random value untuk matriks pertama
    srand(time(NULL));
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_1; j++) {
            mat1[i][j] = rand() % 5 + 1;  // random number between 1-5
        }
    }
```
Kode tersebut digunakan untuk mengisi matriks `mat1` dengan nilai acak yang dihasilkan oleh fungsi `rand()` dari library `stdlib.h`. Fungsi `srand(time(NULL))` digunakan untuk men-generate seed untuk fungsi `rand()` dengan menggunakan waktu saat program dijalankan sebagai acuan. Kemudian, loop for digunakan untuk mengiterasi setiap elemen dalam matriks `mat1`, dan setiap elemen diisi dengan nilai acak yang dihasilkan oleh `rand() % 5 + 1` yang digunakan untuk menghasilkan nilai acak antara 0 dan 4, kemudian ditambahkan 1 sehingga nilai acak yang dihasilkan berada dalam rentang 1 sampai 5.

```shell
// generate random value untuk matriks kedua
    for (i = 0; i < ROW_2; i++) {
        for (j = 0; j < COL_2; j++) {
            mat2[i][j] = rand() % 4 + 1;  // random number between 1-4
        }
    }
```
Kode tersebut digunakan untuk mengisi matriks `mat2` dengan nilai acak yang dihasilkan oleh fungsi `rand()` dari library `stdlib.h`. Loop for digunakan untuk mengiterasi setiap elemen dalam matriks `mat2`, dan setiap elemen diisi dengan nilai acak yang dihasilkan oleh `rand() % 4 + 1` yang digunakan untuk menghasilkan nilai acak antara 0 dan 3, kemudian ditambahkan 1 sehingga nilai acak yang dihasilkan berada dalam rentang 1 sampai 4.

```shell
    // melakukan perkalian matriks
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            result[i][j] = 0;
            for (k = 0; k < ROW_2; k++) {
                result[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }
```
Kode tersebut digunakan untuk melakukan operasi perkalian matriks antara `mat1` dan `mat2`. Loop for digunakan untuk mengiterasi setiap elemen pada matriks hasil result, dan setiap elemen diinisialisasi dengan nilai 0. Kemudian, terdapat loop for dalam loop for yang digunakan untuk mengiterasi setiap elemen pada matriks `mat1` dan `mat2`. Dalam loop tersebut, setiap elemen pada matriks result dihitung sebagai jumlah dari perkalian setiap elemen pada baris `i` dari matriks `mat1` dengan setiap elemen pada kolom `j` dari matriks `mat2`. Hal ini dilakukan dengan melakukan iterasi pada indeks `k` yang mengindikasikan kolom pada `mat1` dan baris pada `mat2` yang sama, sehingga elemen-elemen pada kolom tersebut dapat dikalikan dan dijumlahkan untuk mendapatkan elemen pada posisi `[i][j]` pada matriks result.

```shell
// membuat shared memory
    key_t key = 1234;
    int shmid = shmget(key, sizeof(result), 0666|IPC_CREAT);
    int *shmaddr = (int*) shmat(shmid, (void*)0, 0);
```
Variabel `key` sebagai kunci unik yang akan digunakan untuk mengakses shared memory. Kemudian, menggunakan fungsi `shmget()` untuk membuat shared memory dengan ukuran yang sama dengan result, yaitu matriks hasil perkalian. Fungsi `shmget()` mengembalikan id shared memory yang kemudian disimpan di variabel shmid. Selanjutnya, menggunakan fungsi `shmat()` untuk melekatkan shared memory dengan id shmid ke alamat memori yang tersedia. Variabel `shmaddr` digunakan untuk menunjuk ke alamat awal dari shared memory yang telah melekat. Fungsi `shmat()` juga menerima parameter kedua yang dapat digunakan untuk menentukan lokasi awal dari shared memory (NULL). 

```shell
    // mengcopy hasil perkalian matriks ke shared memory
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            *(shmaddr + i * COL_2 + j) = result[i][j];
        }
    }
```
Code tersebut merupakan proses untuk mengcopy hasil perkalian matriks result ke dalam shared memory dengan menggunakan pointer `shmaddr`. Pada setiap iterasi loop, nilai elemen pada result `[i][j]` akan disalin ke alamat memori yang ditunjuk oleh `shmaddr + i * COL_2 + j`.

```shell
    // print the result matrix
    printf("Result:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
```
Code tersebut berfungsi untuk mencetak hasil perkalian matriks yang disimpan di array result. Dalam loop for, setiap elemen dari matriks hasil akan dicetak menggunakan fungsi `printf()` dengan format `%d`. Baris baru akan ditambahkan setelah setiap baris dari matriks hasil dicetak untuk memastikan keluaran tercetak dengan rapi.

```shell
    // detach shared memory
    shmdt(shmaddr);

    return 0;
}
```
Kode tersebut merupakan proses untuk melepaskan (detach) shared memory dari proses yang menggunakan shared memory tersebut. Pemanggilan fungsi `shmdt(shmaddr)` digunakan untuk melepaskan akses pada alamat memori yang sebelumnya dilampirkan dengan menggunakan `shmat()` dan mengembalikan shared memory ke sistem sehingga bisa digunakan oleh proses lain. Kemudian, fungsi `return 0` akan mengembalikan nilai 0 sebagai tanda bahwa program telah berhasil dijalankan.

#### Point B
#### Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c.

```shell
define ROW_1 4
define COL_2 5
define NUM_THREADS ROW_1 * COL_2
```
Kode `#define` digunakan untuk membuat konstanta. Pada program ini, digunakan `ROW_1` untuk menentukan jumlah baris matriks pertama, `COL_2` untuk menentukan jumlah kolom matriks kedua, dan `NUM_THREADS` untuk menentukan jumlah thread yang akan digunakan pada program. 

```shell
unsigned long long faktorial(int a){
    unsigned long long sum = 1;
    for (int i = 1; i <= a; i++)
    {
        sum = sum * i;
    }
    return sum;
}
```
Code tersebut adalah sebuah fungsi untuk menghitung nilai faktorial dari suatu bilangan bulat positif dengan menggunakan tipe data `unsigned long long` agar dapat menampung nilai faktorial yang besar. Pada awalnya, fungsi `faktorial()` akan menerima sebuah argumen berupa bilangan bulat positif `a`. Kemudian, fungsi akan menginisialisasi sebuah variabel `sum` dengan nilai 1. Fungsi akan melakukan iterasi dari `i = 1` hingga `a` dan pada setiap iterasinya akan mengalikan nilai `sum` dengan `i`. Dengan cara ini, nilai faktorial dari `a` dapat dihitung dan disimpan di dalam variabel `sum`. Setelah selesai melakukan iterasi, fungsi `faktorial()` akan mengembalikan nilai sum yang merupakan nilai faktorial dari bilangan bulat `a`.

```shell
   void *hitung_faktorial(void *arg) {
    int *data = (int *) arg;
    unsigned long long *hasil = malloc(sizeof(unsigned long long));
    *hasil = faktorial(*data);
    pthread_exit(hasil);
}
```
Code tersebut merupakan implementasi dari sebuah thread yang akan menghitung faktorial dari sebuah bilangan yang diberikan sebagai bentuk argumen.

```shell   
int main() {

    clock_t start, end;
    double cpu_time_used;

    start = clock();

    int i, j, rc;
    int shmid;
    key_t key = 1234;
    int (*result)[COL_2];
    pthread_t threads[NUM_THREADS];
    void *status;
    int thread_count = 0;
```
Pada bagian ini, terdapat deklarasi beberapa variabel yang digunakan dalam program, yaitu:

 - `start` dan `end` bertipe `clock_t` digunakan untuk menyimpan waktu mulai dan waktu selesai proses.
- `cpu_time_used` bertipe double digunakan untuk menyimpan selisih waktu yang digunakan dalam proses.
- `i` dan `j` bertipe `int` digunakan sebagai variabel penghitung pada iterasi looping.
- `rc` bertipe `int` digunakan untuk menyimpan nilai kode error pada saat proses pembuatan thread.
- `shmid` bertipe `int` digunakan untuk menyimpan identifier dari shared memory.
- `key` bertipe `key_t` digunakan untuk menyimpan kunci untuk shared memory.
- `result` adalah pointer ke array dua dimensi yang akan menampung hasil perkalian matriks.
- `threads` adalah array dari variabel `pthread_t` yang digunakan untuk menyimpan thread-thread yang akan dibuat.
- status adalah pointer yang akan menampung status dari hasil join thread.
- `thread_count` adalah variabel yang akan digunakan untuk menyimpan jumlah thread yang sudah dibuat.

```shell  
   // get shared memory ID
    if ((shmid = shmget(key, sizeof(int[ROW_1][COL_2]), 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
```
Code tersebut digunakan untuk mendapatkan identitas `(ID)` dari shared memory yang telah dibuat sebelumnya. Fungsi `shmget() `digunakan untuk mengalokasikan shared memory, dan menerima tiga parameter yaitu `key`, `size`, dan `shmflg`. Dalam hal ini, `key` digunakan untuk mengidentifikasi shared memory yang akan digunakan. `size` digunakan untuk menentukan ukuran shared memory dalam byte, dalam hal ini, `sizeof(int[ROW_1][COL_2])` digunakan untuk menentukan ukuran shared memory sebesar `ROW_1` baris dan `COL_2 kolom`, dan `shmflg` digunakan untuk menentukan hak akses ke shared memory.

Jika fungsi `shmget()` gagal mendapatkan `ID` dari shared memory, maka akan menghasilkan error dan program akan berhenti dengan menggunakan fungsi `exit(1)`.

```shell
    // attach shared memory
    if ((result = shmat(shmid, NULL, 0)) == (int (*)[COL_2]) -1) {
        perror("shmat");
        exit(1);
    }
```
Code tersebut adalah bagian dari program yang menggunakan shared memory untuk melakukan operasi perkalian matriks. Pada code ini, dilakukan operasi `attach shared memory` pada program. variabel `resul`t yang akan di-attach pada shared memory yang sudah dibuat sebelumnya menggunakan `shmget()`. `Syntax shmat()` digunakan untuk melekatkan shared memory pada address space dari proses.
Jika operasi `attach` berhasil dilakukan, maka `shmat()` akan mengembalikan alamat dari `shared memory` tersebut, yaitu pointer ke variabel result. Namun jika gagal, `shmat()` akan mengembalikan `(int (*)[COL_2]) -1`. Pada kondisi gagal tersebut, program akan keluar dan menampilkan pesan error.

```shell
// print the result matrix
    printf("Result:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
    printf("\n");
```
Code tersebut berfungsi untuk mencetak nilai dari matriks hasil perkalian yang telah disimpan pada shared memory. Perulangan dilakukan sebanyak `ROW_1` kali untuk baris dan `COL_2` kali untuk kolom, kemudian nilai matriks pada setiap indeks diakses dengan menggunakan operator array multidimensi `[i][j]` dan dicetak ke layar menggunakan fungsi `printf()`. Setelah selesai mencetak semua nilai, terdapat pemanggilan `printf("\n")` untuk membuat baris baru dan memisahkan antara matriks hasil dengan output program lainnya.

#### Point C
#### Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya.

```shell
// hitung faktorial menggunakan thread
    printf("Faktorial:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            int *data = malloc(sizeof(int));
            *data = result[i][j];
            rc = pthread_create(&threads[thread_count], NULL, hitung_faktorial, (void *) data);
            if (rc) {
                printf("Error: return code from pthread_create() is %d\n", rc);
                exit(1);
            }
            thread_count++;
        }
    }
```
Code ini melakukan perhitungan faktorial dari setiap elemen pada matriks hasil perkalian yang telah disimpan di shared memory. Perhitungan faktorial dilakukan menggunakan thread dengan fungsi `hitung_faktorial()`. Setiap elemen pada matriks disimpan dalam variabel data dan diberikan sebagai argumen pada fungsi `hitung_faktorial()` melalui parameter arg. Kemudian, setiap thread diinisialisasi dengan memanggil fungsi `pthread_create()`. Variabel `thread_count` digunakan untuk menghitung jumlah thread yang telah dibuat dan membantu mengindeks array threads.
Setelah semua thread selesai dieksekusi, hasil perhitungan faktorial dari masing-masing elemen disimpan di variabel status dan di `exit` dari setiap thread menggunakan `pthread_exit()`.

```shell
// join semua thread dan print hasil faktorial
    for (i = 0; i < NUM_THREADS; i++) {
        rc = pthread_join(threads[i], &status);
        if (rc) {
            printf("Error: return code from pthread_join() is %d\n", rc);
            exit(1);
        }
        printf("%llu ", *(unsigned long long *) status);
        free(status);
        if ((i + 1) % COL_2 == 0) {
            printf("\n");
        }
    }
```
Code tersebut merupakan bagian dari program yang menghitung faktorial dari masing-masing elemen dalam matriks hasil perkalian yang disimpan di shared memory. Setelah membuat thread-thread untuk menghitung faktorial, program kemudian melakukan join terhadap setiap thread dan mencetak hasil faktorial ke layar.
Pada blok kode tersebut, program melakukan loop untuk melakukan join pada setiap thread yang telah dibuat sebelumnya, dan mengambil nilai balikan dari setiap thread yang disimpan di variabel status. Kemudian, program mencetak nilai faktorial yang diambil dari variabel status menggunakan `%llu`, yang merupakan format specifier untuk `unsigned long long`. Setelah itu, program membebaskan memori yang telah dialokasikan untuk variabel status menggunakan `free()`.

Program juga melakukan pengecekan untuk setiap `COL_2` elemen apakah sudah selesai dicetak atau belum. Jika sudah selesai dicetak, maka program akan mencetak karakter `newline (\n)` untuk membuat baris baru pada layar.

```shell
// detach shared memory
    shmdt(result);
    
    end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("\nWaktu eksekusi: %f detik\n", cpu_time_used);

    return 0;
}
```
Baris kode ini digunakan untuk melepaskan memori bersama yang sebelumnya telah di-attach menggunakan `shmat()`. Kode ini mencegah terjadinya kebocoran memori dan memastikan bahwa memori bersama telah dilepaskan ketika sudah tidak diperlukan lagi.
Selain itu, kode ini juga mencatat waktu eksekusi program menggunakan fungsi `clock()`. Variabel start menyimpan waktu sebelum program dijalankan dan variabel end menyimpan waktu setelah program selesai dieksekusi. Waktu eksekusi kemudian dihitung dengan membagi selisih end - start dengan konstanta `CLOCKS_PER_SEC`. Waktu eksekusi dicetak menggunakan `printf()`. Akhirnya, program mengembalikan nilai 0 sebagai tanda bahwa program telah berhasil dieksekusi tanpa ada kesalahan.

#### Point D
#### Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading.

```shell
unsigned long long faktorial(int a){
    unsigned long long sum = 1;
    for (int i = 1; i <= a; i++)
    {
        sum = sum * i;
    }
    return sum;
}
```
Di dalam function ini, sebuah variabel `sum` diinisialisasi dengan nilai 1 karena nilai faktorial dari 0 dan 1 adalah 1. Lalu, dilakukan looping sebanyak a kali, dimulai dari 1 hingga a. Setiap kali dilakukan looping, nilai variabel `sum` akan dikalikan dengan `i`. Akhirnya, nilai sum akan dikembalikan sebagai nilai hasil faktorial dari a.

```shell
int main() {

    clock_t start, end;
    double cpu_time_used;

    start = clock();

    int i, j;
    int shmid;
    key_t key = 1234;
    int (*result)[COL_2];
```
Code tersebut merupakan bagian dari program untuk mengalikan dua buah matriks. Pada bagian ini, diinisialisasi variabel-variabel yang dibutuhkan dalam program, seperti variabel `i` dan `j` sebagai indeks matriks, `shmid` sebagai `ID` shared memory, `key` sebagai kunci untuk shared memory, dan result sebagai pointer ke matriks hasil perkalian.

Selain itu, juga terdapat inisialisasi variabel `start`, `end`, dan `cpu_time_used` untuk mengukur waktu eksekusi program. Variabel `start`menyimpan waktu awal program dijalankan, variabel `end` menyimpan waktu akhir program dijalankan, dan variabel `cpu_time_used` menyimpan selisih waktu akhir dan awal program dijalankan yang kemudian digunakan untuk menghitung waktu eksekusi program.

```shell
// get shared memory ID
    if ((shmid = shmget(key, sizeof(int[ROW_1][COL_2]), 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
```
Pada bagian ini, program akan meminta untuk mendapatkan `ID` dari shared memory dengan memanggil fungsi `shmget()`. Fungsi ini akan mengalokasikan sebuah blok memori bersama (shared memory) dengan ukuran sebesar `sizeof(int[ROW_1][COL_2])` bytes dengan menggunakan kunci key yang bernilai 1234. Kunci key digunakan sebagai pengenal untuk shared memory tersebut. Jika shared memory belum ada atau tidak berhasil didapatkan, maka akan ditampilkan pesan error `"shmget"` dan program akan keluar dengan memanggil fungsi `exit(1)`. Jika berhasil, maka program akan melanjutkan ke langkah selanjutnya.

```shell
// attach shared memory
    if ((result = shmat(shmid, NULL, 0)) == (int (*)[COL_2]) -1) {
        perror("shmat");
        exit(1);
    }
```
Baris kode tersebut merupakan implementasi untuk meng-attach shared memory dengan `ID` shmid ke dalam program. Fungsi `shmat()` digunakan untuk meng-attach shared memory tersebut pada alamat memori program. Pada baris kode tersebut, variabel result diinisialisasi dengan hasil dari fungsi `shmat(shmid, NULL, 0)`. Parameter pertama pada fungsi `shmat()` merupakan ID shared memory yang ingin di-attach, parameter kedua adalah alamat memori tempat shared memory tersebut di-attach , dan parameter ketiga adalah flag yang digunakan untuk mengatur hak akses terhadap shared memory tersebut. Jika result sama dengan `(int (*)[COL_2]) -1`, maka artinya terjadi kesalahan saat melakukan attach shared memory. Jika terjadi kesalahan, program akan menampilkan pesan error.

```shell
// print the result matrix
    printf("Result:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    printf("Faktorial:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            printf("%llu ", faktorial(result[i][j]));
        }
        printf("\n");
    }
```
Kode di atas mencetak matriks hasil dan menghitung faktorial dari setiap elemen di dalam matriks. Pertama-tama, kode mencetak string `"Result:"` dan kemudian melakukan perulangan melalui baris dan kolom dari matriks hasil menggunakan dua perulangan bersarang. Pada setiap iterasi dari perulangan dalam, kode mencetak elemen matriks yang sesuai menggunakan fungsi printf dengan specifier format `"%d"`, yang mencetak bilangan bulat diikuti oleh spasi. 

Selanjutnya, kode mencetak string "Faktorial:" dan kemudian melakukan perulangan lagi melalui baris dan kolom dari matriks hasil menggunakan dua perulangan bersarang. Pada setiap iterasi dari perulangan dalam, kode memanggil fungsi faktorial dengan elemen matriks yang sesuai sebagai argumen, yang menghitung dan mengembalikan nilai faktorial dari bilangan bulat tersebut. Kode kemudian mencetak nilai faktorial menggunakan fungsi `printf` dengan specifier format `"%llu"`, yang mencetak bilangan bulat tanpa tanda yang sangat besar diikuti oleh spasi.

```shell
// detach shared memory
    shmdt(result);

    end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("\nWaktu eksekusi: %f detik\n", cpu_time_used);
    
    return 0;
}
```
Code tersebut merupakan bagian akhir dari program yang berfungsi untuk mengakhiri penggunaan shared memory dan menghitung waktu eksekusi program. Code di atas digunakan untuk melepaskan shared memory yang telah di-attach sebelumnya dengan menggunakan fungsi `shmat()`. Fungsi `shmdt() ` digunakan untuk melepaskan shared memory tersebut agar bisa digunakan oleh program lain atau bisa dihapus. Code di atas digunakan untuk menghitung waktu eksekusi program. Fungsi `clock()` digunakan untuk mendapatkan jumlah siklus clock yang digunakan program selama berjalan. Setelah mendapatkan jumlah siklus clock, dilakukan pengurangan antara waktu mulai dan waktu selesai program untuk mendapatkan waktu eksekusi program. Selanjutnya waktu eksekusi tersebut dicetak pada layar menggunakan fungsi `printf()`. Terakhir, fungsi `main()` mengembalikan nilai 0 yang menandakan program telah berhasil berakhir.

## NOMOR 3
#### Point A
#### Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.
#### Potongan Code
```shell
//struct untuk message buffer
struct msg_buf{

    int id;
    long msg_type;
    char msg_text[500];
};

int main(){

    struct msg_buf data;
    int msg_id;

    //melakukan inisialisasi bahwa id message queue yaitu 12
    key_t key;
    key = 12;

    //membuat msg_id
    msg_id = msgget(key, 0666|IPC_CREAT);

    if(msg_id == -1){
        puts("Error in creating queue\n");
        exit(0);
    }  

    //handle jika inputan user belum selesai
    int running = 1;
    char buffer_msg[200];

    while(running){

        puts("Masukkan perintah yang sesuai dengan format: ");
        puts("* Untuk menambahkan lagu, ketik \"ADD Judul_lagu\"");
        puts("* Untuk play lagu, ketik \"PLAY \"Judul_Lagu\"\"");
        puts("* Untuk decrypt maka ketik \"DECRYPT\"");
        puts("* Untuk memunculkan daftar lagu, ketik \"LIST\"");
        puts("* Untuk selesai, maka ketik \"end\"");

        //memasukkan inputan ke buffer_msg
        fgets(buffer_msg, 200, stdin);


        //cek apakah stdin berupa perintah DECRYPT, LIST, PLAY, ataupun ADD
        if(strstr(buffer_msg, "DECRYPT") || strstr(buffer_msg, "LIST") || strstr(buffer_msg, "PLAY") || strstr(buffer_msg, "ADD")){
            data.msg_type = 1;

            data.id = getpid();

            strcpy(data.msg_text, buffer_msg);

            if(msgsnd(msg_id, (void*) &data, 500, 0) == -1) puts("Message not sent");
        }

        else if(strstr(buffer_msg, "end")){
            data.id = getpid();
            data.msg_type = 1;

            strcpy(data.msg_text, "end");
            msgsnd(msg_id, (void*) &data, 500, 0);

            exit(EXIT_FAILURE);
        }

        else{
            data.id = getpid();
            data.msg_type = 1;
            
            strcpy(data.msg_text, "UNKNOWN COMMAND");
            msgsnd(msg_id, (void*) &data, 500, 0);
        }

    }




    return 0;
}

/**
 * Referensi:
 * https://dextutor.com/program-for-ipc-using-message-queues/
*/
```
#### Point B
#### User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json (dapat diunduh manual saja melalui link berikut) sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet. Proses decrypt dilakukan oleh program stream.c tanpa menggunakan koneksi socket sehingga struktur direktorinya adalah sebagai berikut (Seperti tercantum di soal)
#### Potongan Code
```shell
void DECRYPT(){

    char* json_data;
    char *filename = "song-playlist.json";
    FILE *file = fopen(filename, "r");

    if(file == NULL){
        puts("Gagal membuka file");
        exit(1);
    }

    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    rewind(file);

    json_data = (char*) malloc(file_size);

    fread(json_data, file_size, 1, file);

    fclose(file);

    char *json_start = strchr(json_data, '[');
    char *json_end = strchr(json_data, ']');

    char *method_start = strstr(json_start, "\"method\"");

    FILE *fp = fopen("playlist.txt", "w");

    while(method_start && method_start < json_end) {

        // Mengambil nilai method
        char *method_value_start = strchr(method_start, ':');
        char *method_value_end = strchr(method_value_start, ',');
        int method_value_len = method_value_end - method_value_start - 3;

        char method[method_value_len + 1];
        strncpy(method, method_value_start + 3, method_value_len);
        method[method_value_len] = '\0';

        strtok(method, "\"");

        printf("Method: %s\n", method);

        // Mengambil nilai song
        char *song_start = strstr(method_value_end, "\"song\"");
        char *song_value_start = strchr(song_start, ':');
        char *song_value_end = strchr(song_value_start, '}');
        long song_value_len = song_value_end - song_value_start - 3;

        char song[song_value_len + 1];
        strncpy(song, song_value_start + 3, song_value_len);
        song[song_value_len] = '\0';

        strtok(song, "\"");

        printf("Song: %s\n", song);
        
        //mulai decrypt:
        if(strcmp(method, "rot13") == 0){
            puts("INI ROT13");

            char *result = rot13(song);
            printf("Hasil decrypt dari rot13: %s\n",result);

            fprintf(fp, "%s\n", result);
        } 

        if(strcmp(method, "base64") == 0){

            char* hasil_decode = base64(song, song_value_len, &song_value_len);

            printf("Hasil decrypt dari base64: %s\n", hasil_decode);

            fprintf(fp, "%s\n", hasil_decode);
        } 

        if(strcmp(method, "hex") == 0){

            char* hexString = hexToASCII(song);

            printf("Hasil hex to string: %s\n", hexString);

            fprintf(fp, "%s\n", hexString);

        } 

        // Pindah ke objek method dan song berikutnya
        method_start = strstr(song_value_end, "\"method\"");
    }

    fclose(fp);
}
```

#### Point  C
#### Selain itu, user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt Sample Output (tercantum di soal)
#### Potongan Code
```shell
void LIST() {

    FILE *file_song = fopen("playlist.txt", "r");
    if (file_song == NULL) {
        puts("File tidak ditemukan!");
    }

    char line[105];
    char lines[1005][105];
    char* uppercaseLines[1005];
    char* lowercaseLines[1005];
    char* numberLines[1005];
    int numUppercaseLines = 0;
    int numLowercaseLines = 0;
    int numNumberLines = 0;

    // Baca setiap baris dan cek huruf pertamanya
    for(int i = 0; i < 1005 && fgets(line, 105, file_song); i++) {
        
        //cek huruf besar
        if (line[0] >= 65 && line[0] <= 90) {
            // Alokasikan memori dan salin isi baris ke array untuk huruf besar
            uppercaseLines[numUppercaseLines] = lines[i];
            strcpy(uppercaseLines[numUppercaseLines], line);
            numUppercaseLines++;
        } 
        //cek huruf kecil
        else if (line[0] >= 97 && line[0] <= 122) {
            // Alokasikan memori dan salin isi baris ke array untuk huruf kecil
            lowercaseLines[numLowercaseLines] = lines[i];
            strcpy(lowercaseLines[numLowercaseLines], line);
            numLowercaseLines++;
        } 
        //cek angka dsb
        else{
            // Alokasikan memori dan salin isi baris ke array untuk angka
            numberLines[numNumberLines] = lines[i];
            strcpy(numberLines[numNumberLines], line);
            numNumberLines++;
        }
    }
```

#### Point  D
#### User juga dapat mengirimkan perintah PLAY <SONG> dengan ketentuan sebagai berikut.(tercantum di soal)
#### Potongan Code
```shell
void PLAY(const int x, const char* words){

    FILE * cek_song;
    char line_cek[150];
    char matchedLines[100][150];
    long numMatched = 0;

    cek_song = fopen("playlist.txt", "r");

    while(fgets(line_cek, 150, cek_song)){

        char* tmp = strdup(line_cek);

        for(int i = 0; i < strlen(tmp); i++){
            tmp[i] = tolower(tmp[i]);
        }

        char* wordLower = strdup(words);

        for(int i = 0; i < strlen(wordLower); i++){
            wordLower[i] = tolower(wordLower[i]);
        }

        if(strstr(tmp, wordLower)){
            
            strcpy(matchedLines[numMatched], line_cek);
            numMatched++;
        }
    }

    fclose(cek_song);

    if(numMatched == 1) printf("USER %d PLAYING \"%s\"\n", x, matchedLines[0]);
    else if(numMatched > 1){
        printf("THERE ARE %ld SONG CONTAINING \"%s\":", numMatched, words);
        puts("");
        
        for(long i = 0; i < numMatched; i++){
            printf("%ld. %s", i + 1, matchedLines[i]);
        }
    }
    else printf("THERE IS NO SONG CONTAINING \"%s\"\n", words);

}
```

#### Point  E
#### User juga dapat menambahkan lagu ke dalam playlist dengan syarat sebagai berikut (tercantum di soal)
#### Potongan Code
```shell

```

#### Point  F
#### Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya dapat memiliki dua user. Gunakan semaphore (wajib) untuk membatasi user yang mengakses playlist. Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.
#### Potongan Code
```shell
void ADD(const int x, const char* newSong){

    FILE *add_song;
    add_song = fopen("playlist.txt", "r");

    char line[150];
    int flag = 0;
    while(fgets(line, 150, add_song)){

        if(strstr(line, newSong)){
            flag = 1;
            break;
        }
    }

    fclose(add_song);

    if(flag == 0){
        add_song = fopen("playlist.txt", "a");

        fputs(newSong, add_song);

        fclose(add_song);

        printf("USER %d ADD %s\n", x, newSong);
    }

    else puts("SONG ALREADY ON PLAYLIST");
}
```

#### Point  G
#### Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".
#### Potongan Code
```shell
else{
            data.id = getpid();
            data.msg_type = 1;
            
            strcpy(data.msg_text, "UNKNOWN COMMAND");
            msgsnd(msg_id, (void*) &data, 500, 0);
        }
```


## NOMOR 4
#### Point A
#### Download dan unzip file tersebut dalam kode c bernama unzip.c.

```shell
int main(void)
{
    //download
    system("wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp' -O hehe.zip");
```    
Pemanggilan fungsi `system()` digunakan untuk mengeksekusi perintah di shell atau terminal. Dalam hal ini, perintah yang dieksekusi adalah `"wget"`, yang merupakan utilitas yang umum digunakan untuk mengunduh file dari internet. Opsi yang digunakan dengan `wget` adalah:

- `"--no-check-certificate"`: Ini memberitahu wget untuk tidak memverifikasi sertifikat SSL / TLS dari server yang diunduh. Ini dapat berguna jika sertifikatnya self-signed atau tidak valid.
- `"'https://docs.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp'"`: Ini adalah URL dari file yang diunduh. Tampaknya file tersebut di-hosting di Google Drive, dan parameter "&id=" menentukan pengenal unik untuk file tersebut.
- `"-O hehe.zip"`: Ini menentukan nama file untuk menyimpan file yang diunduh. Dalam hal ini, itu adalah "hehe.zip".
Secara keseluruhan, kode ini mencoba untuk mengunduh file dari Google Drive dan menyimpannya sebagai "hehe.zip".

```shell
    // check if downloaded file exists and has a valid size
    FILE *file = fopen("hehe.zip", "r");
    if (file == NULL) {
        printf("Error: downloaded file not found\n");
        return 1;
    }
```    
Code tersebut digunakan untuk memeriksa apakah file yang diunduh sudah ada dan memiliki ukuran yang valid. Kode ini membuka file `"hehe.zip"`  menggunakan fungsi `fopen()` dan mode `"r" (read)` untuk membaca file yang sudah diunduh. Jika file tidak ditemukan, maka akan muncul pesan kesalahan dan program akan mengembalikan nilai 1 untuk menandakan kegagalan.

```shell
fseek(file, 0, SEEK_END);
    long size = ftell(file);
    if (size < 1024) {
        printf("Error: downloaded file size is too small (%ld bytes)\n", size);
        return 1;
    }
    fclose(file);
```
Setelah file dibuka, kode menggunakan fungsi `fseek()` dan `ftell()` untuk menghitung ukuran file dalam byte. `Fseek()` digunakan untuk memindahkan posisi baca atau tulis dalam file ke akhir file menggunakan nilai offset 0 dan mode `SEEK_END`. `Ftell()` kemudian mengembalikan ukuran file dalam byte. Setelah ukuran file dihitung, kode memeriksa apakah ukuran file kurang dari 1024 byte (1 KB). Jika iya, maka akan muncul pesan kesalahan dan program akan mengembalikan nilai 1 untuk menandakan kegagalan.

```shell
    //unzip
    system("unzip hehe.zip");

    //remove hehe.zip
    system("rm -rf hehe.zip");
    return 0;
}
```
Untuk `"unzip hehe.zip"` yang mengekstrak isi file ZIP `"hehe.zip"` ke dalam direktori kerja saat ini. Jadi, kode ini digunakan untuk mengekstrak file ZIP tersebut agar bisa digunakan oleh program selanjutnya. Untuk `"rm -rf hehe.zip"` yang digunakan untuk menghapus file ZIP `"hehe.zip"` dari sistem. Dalam hal ini, penggunaan `"rm"` adalah untuk menghapus file, dan `"-rf"` adalah untuk menghapus secara rekursif dan mengabaikan pesan peringatan yang muncul.

#### Point B
#### Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other. Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.
#### Potongan Code
```shell
    char filename[50] = "extensions.txt";
    char dirname[MAX_LEN];
    char other_dirname[MAX_LEN] = "other";
    FILE *fp;
    struct stat st;
    pthread_t tid[MAX_LEN];
    struct ThreadArgs thread_args[MAX_LEN];
    int i = 0;

    // membuka file untuk dibaca
    fp = fopen(filename, "r");

    // memeriksa apakah file berhasil dibuka
    if (fp == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    // membuat direktori "categorized" jika belum ada
    if (stat("categorized", &st) != 0) {
        mkdir("categorized", 0777);
        writeLogMade("categorized");
    }

    // membuat direktori "other" di dalam "categorized" jika belum ada
    if (stat("categorized/other", &st) != 0) {
        writeLogMade("categorized/other");
        if (mkdir("categorized/other", 0777) != 0)
            printf("Gagal membuat direktori 'other' di dalam 'categorized'.\n");
    }

    char maxfilename[50] = "max.txt";
    //open max.txt
    FILE *max;
    // membuka file untuk dibaca
    max = fopen(maxfilename, "r");

    // memeriksa apakah file berhasil dibuka
    if (max == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    int maxFile;
    fscanf(max, "%d", &maxFile); // read the first line of max.txt into firstLine

     // menutup file
    fclose(max);
```

#### Point C
#### Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
```shell
extension_a : banyak_file
extension_b : banyak_file
extension_c : banyak_file
other : banyak_file
```
#### Potongan Code
```shell
    // Mengeksekusi command dan membuka stream untuk membaca outputnya
    FILE* fp;
    fp = popen(cmd, "r");
    if (fp == NULL) {
        printf("Error: Failed to execute command.\n");
    }

    // Membaca output dari stream dan menampilkannya ke layar
    writeLogAccess(hehe_dir);
    fgets(countFile, sizeof(countFile), fp);
    int numFiles = atoi(countFile);

    // ascending output
    char _write[MAX_LEN];
    system("touch buffer.txt");
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);
    system(_write);
    
    //menghitung total direktori yang dibuat per ext [@todo, check if maxFile=0]
    int numDir = numFiles / maxFile ;

    if (numFiles % maxFile > 0)
        numDir = numDir + 1;

    // Menutup stream dan mengembalikan nilai 0 (sukses)
    pclose(fp);

    //create dir  if numDir == 0
    if (numDir == 0 && mkdir(new_dirname, 0777) == 0) {
    // directory creation was successful, so do something
        writeLogMade(new_dirname);
    }

    for (int i = 0; i < numDir; i++)
    {
         // mendefinisikan variable nama directory dengan suffix sesuai dengan index
         char _dirName[30];
         strcpy(_dirName, new_dirname);
         if(i > 0) {
            char suffix[10];
            sprintf(suffix, " (%d)", i+1);
            strcat(_dirName, suffix);
        }       
```


#### Point D
#### Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.
#### Potongan Code
```shell
pthread_create(&tid[i], NULL, create_directory, &thread_args[i]);
```

#### Point E
Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.
DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
DD-MM-YYYY HH:MM:SS MADE [folder name]
examples : 
02-05-2023 10:01:02 ACCESSED files
02-05-2023 10:01:03 ACCESSED files/abcd
02-05-2023 10:01:04 MADE categorized
02-05-2023 10:01:05 MADE categorized/jpg
02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg
Catatan:
- Path dimulai dari folder files atau categorized
- Simpan di dalam log.txt
- ACCESSED merupakan folder files beserta dalamnya
- Urutan log tidak harus sama

#### Potongan Code
```shell
sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);
                    
                    sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);
                    sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFile, command2);
```

#### Point F
#### Untuk mengecek apakah log-nya benar, buatlah program dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan yang ada

```shell
int main() {
    // Accessed count
    printf ("\e[33mTotal banyak akses:\e[0m\n");
    system("grep ACCESSED log.txt | wc -l");
```
Program ini akan mencetak jumlah baris dalam file `"log.txt"` yang mengandung kata `"ACCESSED"`. Pertama-tama, program mencetak pesan "Total banyak akses:" di terminal menggunakan `printf()`. "\e[33m" digunakan untuk memberikan warna kuning pada teks, dan "\e[0m" digunakan untuk mengembalikan warna ke default (hitam putih).

Setelah itu, program menggunakan fungsi system() untuk mengeksekusi perintah shell `"grep ACCESSED log.txt | wc -l"`. Perintah ini akan mencari baris yang mengandung kata "ACCESSED" di dalam file `"log.txt"` menggunakan perintah grep. Kemudian, `"|"` digunakan untuk mengalirkan hasil dari perintah grep ke perintah `wc (word count)`. Opsi `"-l"` pada wc digunakan untuk menghitung jumlah baris, sehingga menghasilkan jumlah baris dalam file `"log.txt"` yang mengandung kata `"ACCESSED"`. Hasil dari perintah `wc` akan dicetak di layar oleh program setelah perintah `system()` dieksekusi. Setelah itu, program akan mengembalikan nilai 0 untuk menandakan bahwa program selesai dengan sukses.

```shell
// List folders & total file
    printf ("\e[33m\nList folder & total file tiap folder:\e[0m\n");
    system("grep 'MOVED\\|MADE' log.txt \
    | awk -F 'categorized' '{print \"categorized\"$NF}' \
    | sed '/^x$/d' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \"$3 \"=\" \" \" $1-1}'");
```
Kode tersebut digunakan untuk mencari dan menampilkan daftar folder serta total file di dalam setiap folder. Pada dasarnya, kode ini melakukan operasi sebagai berikut:

    - Mengambil baris dari file log.txt yang mengandung kata "MADE" atau "MOVED".
    - Mengambil string setelah kata "categorized" pada setiap baris menggunakan perintah awk.
    - Menghapus baris yang hanya berisi karakter "x" menggunakan perintah sed.
    - Mengurutkan daftar folder secara alfabetis menggunakan perintah sort.
    - Menghitung jumlah unik folder menggunakan perintah uniq dan menyimpannya dalam variabel jumlah file.
    - Menampilkan daftar folder dan jumlah file di dalam setiap folder menggunakan perintah awk.

```shell
   // total file tiap extension terurut secara ascending
    printf ("\n\e[33mTotal file tiap extension\e[0m\n");
    system("grep 'MADE\\|MOVED' log.txt \
    | grep -o 'categorized/[^o]*' \
    | sed 's/categorized\\///' \
    | sed '/^$/d' \
    | sed 's/ \\([^o]*\\)//' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \" \"=\" \" \" $1-1}'");

    return 0;
}
```
Kode tersebut digunakan untuk mencari dan menampilkan total file untuk setiap ekstensi file yang ada dalam `log.txt`. Pada dasarnya, kode ini melakukan operasi sebagai berikut:

    - Mengambil baris dari file log.txt yang mengandung kata "MADE" atau "MOVED".
    - Mengambil string setelah "categorized/" pada setiap baris menggunakan perintah grep dan grep -o.
    - Menghapus kata "categorized/" pada setiap baris menggunakan perintah sed.
    - Menghapus baris yang kosong atau hanya mengandung karakter "o" menggunakan perintah sed.
    - Mengambil string sebelum karakter "o" pada setiap baris menggunakan perintah sed.
    - Mengurutkan daftar ekstensi secara alfabetis menggunakan perintah sort.
    - Menghitung jumlah unik ekstensi file menggunakan perintah uniq dan menyimpannya dalam variabel jumlah file.
    - Menampilkan daftar ekstensi file dan jumlah file untuk setiap ekstensi file menggunakan perintah awk.








