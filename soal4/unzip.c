#include <stdlib.h>
#include <stdio.h>

int main(void)
{
    //download
    system("wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp' -O hehe.zip");
    
    // check if downloaded file exists and has a valid size
    FILE *file = fopen("hehe.zip", "r");
    if (file == NULL) {
        printf("Error: downloaded file not found\n");
        return 1;
    }
    fseek(file, 0, SEEK_END);
    long size = ftell(file);
    if (size < 1024) {
        printf("Error: downloaded file size is too small (%ld bytes)\n", size);
        return 1;
    }
    fclose(file);

    //unzip
    system("unzip hehe.zip");

    //remove hehe.zip
    system("rm -rf hehe.zip");
    return 0;
}
